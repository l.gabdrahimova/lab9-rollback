import DB from "./DB";

(async function (){
    const db = new DB({database: 'company'});
    await db.connect();

    await db.addSalaries([1,2,3],[1,2,3],[100, 200])
    await db.printSalaries()
    // Success
    // 0)  {"id":1,"employee_id":1,"amount":100}
    // 1)  {"id":2,"employee_id":2,"amount":200}

    await db.addSalaries([2,3],[2,3],[2000, 300])
    await db.printSalaries()
    // Success
    // 0)  {"id":1,"employee_id":1,"amount":100}
    // 1)  {"id":2,"employee_id":2,"amount":200}
    // 2)  {"id":3,"employee_id":3,"amount":300}

    await db.addSalaries([4, 5],[4,5],[400, 'nonsense'])
    await db.printSalaries()
    // Rollback, because of error:
    //     column "nonsense" does not exist
    // 0)  {"id":1,"employee_id":1,"amount":100}
    // 1)  {"id":2,"employee_id":2,"amount":200}
    // 2)  {"id":3,"employee_id":3,"amount":300}


    await db.disconnect()
})()
