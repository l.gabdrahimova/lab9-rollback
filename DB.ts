import {Client, ClientConfig} from "pg";


export default class DB {
    private client: Client;

    constructor(config: ClientConfig) {
        this.client = new Client(config)
    }

    async connect() {
        await this.client.connect()
    }

    async disconnect() {
        await this.client.end()
    }

    async addSalaries(ids: any[], employeeIDs: any[], amounts: any[]): Promise<boolean> {
        await this.client.query('BEGIN')
        const N = Math.min(ids.length, employeeIDs.length, amounts.length);
        for (let i = 0; i < N; i++) {
            const id = ids[i]
            const employeeId = employeeIDs[i]
            const amount = amounts[i]

            const result = await this.client.query(`SELECT id FROM Salary WHERE employee_id=${employeeId} or id=${id} LIMIT 1;`)
            if(!result.rows.length){
                try {
                    await this.client.query(`INSERT INTO Salary VALUES (${id},${employeeId},${amount});`)
                }catch (e) {
                    console.log(`Rollback, because of error:\n${e.message}`)
                    await this.client.query("ROLLBACK");
                    return false
                }
            }
        }

        await this.client.query("COMMIT");
        console.log('Success')
        return true
    }

    async printSalaries(){
        const result = await this.client.query(`SELECT * FROM Salary;`)
        console.log(result.rows.map((row, index) => `${index})  ${JSON.stringify(row)}`).join('\n'))
    }
}

